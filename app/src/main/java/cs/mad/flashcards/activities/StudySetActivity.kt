package cs.mad.flashcards.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class StudySetActivity : AppCompatActivity() {
    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_set)
        var flash_set = Flashcard.getHardcodedFlashcards().toMutableList()
        var missed_cards = mutableListOf<Flashcard>()
        var num_missed = 0
        var num_correct = 0
        var num_completed = 0
        var isTerm = true
        var lenth = flash_set.size

        //Exit button:
        val exit_button: Button = findViewById(R.id.exit)
        exit_button.setOnClickListener(){
            onBackPressed()
        }

        // total compelted:
        val total_completed: TextView = findViewById(R.id.total_completed)
        total_completed.text = "Completed: " + num_completed + "/" + lenth


        // Textview:
        val studyCard: TextView = findViewById(R.id.terms)
        studyCard.text = flash_set[0].term

        // Term/Defintion:
        studyCard.setOnClickListener({
            if (isTerm == true){
                isTerm = false
                studyCard.text = flash_set[0].definition
            }
            else{
                isTerm = true
                studyCard.text = flash_set[0].term
            }
        })

        // missed button
        val missed_text: TextView = findViewById(R.id.missed_count)
        val missed_button: Button = findViewById(R.id.missed)

        missed_button.setOnClickListener({
            num_missed++
            missed_text.text = "missed: " + num_missed
            var missedCard = flash_set[0]
            missed_cards.add(missedCard)
            flash_set.remove(missedCard)
            flash_set.add(missedCard)
            studyCard.text = flash_set[0].term

        })

        // Correct button
        val correct_text: TextView = findViewById(R.id.correct_count)
        val correct_button: Button = findViewById(R.id.correct)

        correct_button.setOnClickListener({
            var correctCard = flash_set[0]
            num_completed++
            if (!missed_cards.contains(correctCard)){
                num_correct++
            }
            correct_text.text = "correct: " + num_correct
            flash_set.remove(correctCard)
            studyCard.text = flash_set[0].term
            total_completed.text = "Completed: " + num_completed + "/" + lenth
        })


        //Skip button:
        val skip_button: Button = findViewById(R.id.skip)
        skip_button.setOnClickListener({
            var skippedCard = flash_set[0]
            flash_set.remove(skippedCard)
            flash_set.add(skippedCard)
            studyCard.text = flash_set[0].term
        })








    }
}