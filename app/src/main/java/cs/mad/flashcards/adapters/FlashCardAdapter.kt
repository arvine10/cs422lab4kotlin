package cs.mad.flashcards.adapters



import android.app.AlertDialog
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard
import android.text.Editable
import android.text.InputType
import android.widget.EditText


class FlashCardAdapter(flashCard: List<Flashcard>) :
    RecyclerView.Adapter<FlashCardAdapter.ViewHolder>() {
    private var data =  flashCard.toMutableList()



    fun createDialgue(viewHolder: ViewHolder, item: Flashcard){
        val builder2 = AlertDialog.Builder(viewHolder.itemView.context)
        builder2.setTitle(item.term + " " + item.definition)


        val dialoge_view = LayoutInflater.from(viewHolder.itemView.context).inflate(R.layout.dialog,null)
        builder2.setView(dialoge_view)

        val input1 = dialoge_view.findViewById<EditText>(R.id.input1)
        val input2 = dialoge_view.findViewById<EditText>(R.id.input2)
        builder2.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            // Here you get get input text from the Edittext

            item.term = input1.text.toString()
            item.definition = input2.text.toString()
            notifyDataSetChanged()
        })

            .create().show()
    }



    fun addnew(flashcard: Flashcard){
        data.add(flashcard)
        notifyItemInserted(data.size)
    }

    // need to get and store a reference to the data set

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.text_view)

//        val button: Button = view.findViewById(R.id.add)

        // store view references as properties using findViewById on view
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        /*
            this is written for you but understand what is happening here
            the layout for an individual item is being inflated
            the inflated layout is passed to view holder for storage

            THE ITEM LAYOUT STILL NEEDS TO BE SETUP IN THE LAYOUT EDITOR
         */
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        var item = data[position]
        viewHolder.textView.text = item.term




        // do onLongClickListner next...
        viewHolder.textView.setOnLongClickListener{
            createDialgue(viewHolder,item)
             true

        }


        viewHolder.textView.setOnClickListener({
            val builder = AlertDialog.Builder(viewHolder.itemView.context)
            val title = item.term + " " + item.definition
            builder.setTitle(title)


            builder.setPositiveButton("Edit", DialogInterface.OnClickListener({ dialog: DialogInterface, which: Int ->
               dialog.dismiss()
                createDialgue(viewHolder,item)



            })).create().show()
        })

//        viewHolder.button.setOnClickListener({
//            val flashCard = Flashcard("new","def")
//            data.add(flashCard)
//
//        })



        /*
            fill the contents of the view using references in view holder and current position in data set

            to launch FlashcardSetDetailActivity ->
            viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
         */

    }

    override fun getItemCount(): Int {
        // return the size of the data set

        return data.size
    }
}
